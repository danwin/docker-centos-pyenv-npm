FROM centos:7

MAINTAINER dmitry.zimoglyadov@gmail.com

ARG pythonversion=3.5.3

ENV USER dev_user
ENV HOME  /home/${USER}
ENV PYENV_ROOT ${HOME}/.pyenv
ENV PATH ${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:/usr/local/mysql/bin:$PATH

# Install tools
RUN yum -y install epel-release \
    && yum -y update \
    && yum -y install gcc git curl make zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl openssl-devel mysql-devel mysqlclient-devel nodejs npm\
    yum -y autoremove &&\
    yum clean all &&\
    rm -rf /var/cache/yum

# User management
RUN useradd -m ${USER} &&\
    mkdir /var/www &&\
    chmod -R u+rw /var/www &&\
    chmod -R a+r /var/www

USER ${USER}
WORKDIR ${HOME}

RUN git clone git://github.com/yyuu/pyenv.git .pyenv \
    && pyenv install ${pythonversion} \
    && pyenv global ${pythonversion} \
    && pyenv rehash

CMD [ "bash" ]